class Product < ActiveRecord::Base
  belongs_to :inventory
  belongs_to :category
end
