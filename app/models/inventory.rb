class Inventory < ActiveRecord::Base
  
  belongs_to :family
  has_many   :products
  has_many   :categories, through: :products

end
