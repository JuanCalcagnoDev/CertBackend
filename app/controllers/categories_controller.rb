class CategoriesController < ApplicationController  

  def index
    categories = Category.all

    if categories
      render json: categories
    else
      render json:{message: '404'}
    end

  end

  def show

    category = Category.find(params[:id])

    if category
      render json: category
    else
      render json:{message: '404 - Not found'}
    end

  end

category = Category.create(permit_params)

    if category.save
      render json: category
    else
      render json:{message: 'Incorrect Params'}
    end

  private

  def permit_params
    params.require(:product).permit(:categoryName)
  end

end