class ProductsController < ApplicationController  


  def index
    products = Product.where(category_id: params[:category_id])

    if products
      render json: products
    else
      render json:{message: '404'}
    end

  end   

  def all_products
    products = Product.all

    if products
      render json: products
    else
      render json:{message: '404'}
    end

  end

  def show

    product = Product.find(params[:id])

    if product
      render json: product
    else
      render json:{message: '404 - Not found'}
    end

  end

  def create
    
    product = Product.create(product_params)

    if product.save
      render json: product
    else
      render json:{message: 'Incorrect Params'}
    end

  end

  private

  def product_params
    params.require(:product).permit(:name,:price,:stock,:inventory_id,:category_id)
  end

end