class FamiliesController < ApplicationController  


  def index
    
    families = Family.all
    if families
      render json: families
    else
      record_not_found( "Failed Resource" )
    end

  end

  def show

    family = Family.find(params[:id])
    if family
      render json: family
    else
      record_not_found( "Failed Resource" )
    end

  end

  def create

    family = Family.create(family_params)

    if family.save
      render json: family
    else
      render json:{message: 'Incorrect Params'}
    end
    
  end

  private

  def family_params
    params.require(:family).permit(:name)
  end

end