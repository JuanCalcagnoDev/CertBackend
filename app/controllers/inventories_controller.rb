class InventoriesController < ApplicationController  
 
 def index
    
    inventories = Inventory.all

    if inventories
      render json: inventories
    else
      record_not_found( "Failed Resource" )
    end

  end

  def show

    inventory = Inventory.find(params[:id])
    
    if inventory
      render json: inventory
    else
      record_not_found( "Failed Resource" )
    end

  end

  def create

    inventory = Inventory.create(inventory_params)

    if inventory.save
      render json: inventory
    else
      render json:{message: 'Incorrect Params'}
    end
    
  end

  private

  def inventory_params
    params.require(:inventory).permit(:typeOf,:family_id)
  end

end