Rails.application.routes.draw do

  resources :families    , only: [:index,:show,:create,:update,:destroy]
  resources :inventories , only: [:index,:show,:create,:update,:destroy]
  resources :categories  , only: [:index,:show,:create,:update,:destroy] do

    resources :products     , only: [:index,:show,:create,:update,:destroy]
  end

 get   "/products",                                      to: "products#all_products"
 post  "/products",                                      to: "products#create"

end
