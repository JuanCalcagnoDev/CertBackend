class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :typeOf
      t.references :family, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
